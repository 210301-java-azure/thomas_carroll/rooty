document.getElementById("new-item-form").addEventListener("submit", addNewItem);

function addNewItem(e){
    e.preventDefault();
    const plantName = document.getElementById("new-plant-name").value;
    const owner = document.getElementById("new-plant-owner").value;
    const soil
    const newItem = {
    "plantName": plantName,
    "owner": owner,
    "soilUsed": "10-10-10",
    "fertilizerUsed": "miraclegro",
    "daysBetweenWatering": 3,
    "weeksBetweenFeeding": 2,
    "userCaption": "fun plant",
    "leafsUpvotes": 10
    }
    // javascript object (newItem) -> json -> java object (MarketItem.class)
    ajaxCreateItem(newItem, indicateSuccess, indicateFailure);
}

function indicateSuccess(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "New item successfully created";
}

function indicateFailure(){
    const message = document.getElementById("create-msg"); 
    message.hidden = false;
    message.innerText = "There was an issue creating your new item";
}
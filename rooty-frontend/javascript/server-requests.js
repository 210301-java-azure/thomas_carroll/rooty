
function sendAjaxRequest(method, url, body, successCallback, failureCallback, authToken){
    const xhr = new XMLHttpRequest(); // ready state 0 
    xhr.open(method, url); // ready state 1
    if(authToken){
        xhr.setRequestHeader("JWT", authToken);
    }
    xhr.onreadystatechange = function(){
        if(xhr.readyState == 4){
            if(xhr.status>199 && xhr.status<300){
                successCallback(xhr);
            } else {
                failureCallback(xhr);
            }
        }
    }
    if(body){
        xhr.send(body);
    } else {
        xhr.send(); // ready state 2,3,4 follow
    }
}

function sendAjaxPost(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("POST", url, body, successCallback, failureCallback, authToken);
}
function ajaxUpdate(url, body, successCallback, failureCallback, authToken){
    sendAjaxRequest("PUT", url, body, successCallback, failureCallback, authToken);
}
function ajaxDelete(url, successCallback, failureCallback, authToken){
    sendAjaxRequest("DELETE", url, null, successCallback, failureCallback, authToken);
}


function ajaxLogin(username, password, successCallback, failureCallback){
    var data = {"username": username, "password": password, "state": "null"};
    const itemJson = JSON.stringify(data);
    sendAjaxPost("http://13.89.229.166/login", itemJson, successCallback, failureCallback);
}

function ajaxRegister(username, password, successCallback, failureCallback){
    var data = {"username": username, "password": password};
    const itemJson = JSON.stringify(data);
    sendAjaxPost("http://13.89.229.166/register", itemJson, successCallback, failureCallback);
}



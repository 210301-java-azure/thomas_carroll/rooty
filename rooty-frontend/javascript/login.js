if(sessionStorage.getItem("token")){
    document.getElementById("login-dropdown").hidden=true
    document.getElementById("dropdownmenu").hidden=false
    document.getElementById("dropdownMenuButton").innerHTML = sessionStorage.getItem("user");
};

document.getElementById("login-form").addEventListener("submit", preventDefaultOf);


function preventDefaultOf(event){
    event.preventDefault();
}



function attemptLogin(){
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    ajaxLogin(username, password, successfulLogin, loginFailed)
}

function attemptRegister(){
    const username = document.getElementById("inputUsername").value;
    const password = document.getElementById("inputPassword").value;
    ajaxRegister(username, password, successfulLogin, loginFailed)
}

function successfulLogin(xhr){
    console.log("yay success");
    const authToken = xhr.getResponseHeader("JWT");
    const userToken = xhr.getResponseHeader("USERNAME");
    sessionStorage.setItem("token", authToken);
    sessionStorage.setItem("user", userToken);
    window.location.href = "./index.html";
}

function loginFailed(xhr){
    // console.log("oh no something went wrong");
    const errorDiv = document.getElementById("error-msg");
    errorDiv.hidden = false;
    errorDiv.innerText = "Username already taken :(";
}

function logoutUser(){

    sessionStorage.removeItem("token");
    sessionStorage.removeItem("user");
    location.reload();
}
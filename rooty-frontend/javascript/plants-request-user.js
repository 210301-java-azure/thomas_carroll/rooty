// getting item data from our server
// document.getElementById("plant-table-body").addEventListener("input", changeToText);


const xhr = new XMLHttpRequest(); // ready state 0 
let username = sessionStorage.getItem("user");
xhr.open("GET", "http://13.89.229.166/profile/" + username); // ready state 1
// let token = sessionStorage.getItem("jwt"); // we could set the Auth header with this value instead
let token = sessionStorage.getItem("token");
console.log(token)
xhr.setRequestHeader("JWT", token.toString());
xhr.onreadystatechange = function(){
    if(xhr.readyState == 4){
        if(xhr.status == 200){
            console.log("works")
            const items = JSON.parse(xhr.responseText);
            renderItemsInTable(items);
        } else {
            console.log("something went wrong with your request")
        }
    }
}
xhr.send(); // ready state 2,3,4 follow 


function renderItemsInTable(itemsList){
    document.getElementById("plants-table").hidden = false;
    const tableBody = document.getElementById("plants-table-body");
    var i = 0;
    for(let item of itemsList){
        let newRow = document.createElement('tr');
        newRow.setAttribute('id','row${i}')
        newRow.innerHTML = `<td id="${i}plant-name">${item.plantName}</td><td id="${i}soil-used">${item.soilUsed}</td>
                            <td id="${i}fertilizer-name">${item.fertilizerUsed}</td><td id="${i}days-watering">${item.daysBetweenWatering}</td>
                            <td id="${i}weeks-feeding">${item.weeksBetweenFeeding}</td><td id="${i}user-caption">${item.userCaption}</td>
                            <td id="${i}leafs-vote" contenteditable="false">${item.leafsUpvotes}</td><td id="${i}plant-id" contenteditable="false">${item.plantId}</td>
                            <td><button contenteditable="false" style="margin:0;padding-top:0;" class="btn btn-sm btn-primary btn-block" id="btn${i}"  onclick="attemptUpdate(this.id)">Update</button></td>
                            <td><button contenteditable="false" style="margin:0;padding-top:0;" class="btn btn-sm btn-primary btn-block" id="btn${i}"  onclick="attemptDelete(this.id)">Delete</button></td>`;
        tableBody.appendChild(newRow);
        i++;
    }    
}

function changeToText(event){
    console.log(event)
}

function attemptDelete(clicked_id){
    var row = clicked_id.charAt(3)
    var id = document.getElementById(row + "plant-id").innerText
    let token = sessionStorage.getItem("token")
    ajaxDelete("http://13.89.229.166/plants/" + id, successfulDelete, failureDelete, token)
}

function attemptCreate(clicked_id){

}

function attemptUpdate(clicked_id){
    var row = clicked_id.charAt(3)
    var newplantId = parseInt(document.getElementById(row + "plant-id").innerText)
    var newplantName = document.getElementById(row + "plant-name").innerText
    var newsoilUsed = document.getElementById(row + "soil-used").innerText
    var newfertilizer = document.getElementById(row + "fertilizer-name").innerText
    var newwatering = parseInt(document.getElementById(row + "days-watering").innerText)
    var newfeeding = parseInt(document.getElementById(row + "weeks-feeding").innerText)
    var newcaption = document.getElementById(row + "user-caption").innerText
    var newleafsUpvotes = parseInt(document.getElementById(row + "leafs-vote").innerText)
    var newowner = sessionStorage.getItem("user")
    let newtoken = sessionStorage.getItem("token")
    let newItem = {
        plantId: newplantId,
        plantName: newplantName,
        owner:{username: newowner},
        soilUsed: newsoilUsed,
        fertilizerUsed: newfertilizer,
        daysBetweenWatering: newwatering,
        weeksBetweenFeeding: newfeeding,
        userCaption: newcaption,
        leafsUpvotes: newleafsUpvotes
    };
    payload = JSON.stringify(newItem)
    ajaxUpdate("http://13.89.229.166/plants/" + newplantId, payload, successfulUpdate, failureUpdate, newtoken)
}

function attemptCreate(clicked_id){
    var row = clicked_id.charAt(3)
    var newplantName = document.getElementById("new-plant-name").innerText
    var newsoilUsed = document.getElementById("new-soil-used").innerText
    var newfertilizer = document.getElementById("new-fertilizer-name").innerText
    var newwatering = parseInt(document.getElementById("new-days-watering").innerText)
    var newfeeding = parseInt(document.getElementById("new-weeks-feeding").innerText)
    var newcaption = document.getElementById("new-user-caption").innerText
    var newowner = sessionStorage.getItem("user")
    let newtoken = sessionStorage.getItem("token")
    let newItem = {
        plantName: newplantName,
        owner:{username: newowner},
        soilUsed: newsoilUsed,
        fertilizerUsed: newfertilizer,
        daysBetweenWatering: newwatering,
        weeksBetweenFeeding: newfeeding,
        userCaption: newcaption,
        leafsUpvotes: null
    };
    payload = JSON.stringify(newItem)
    sendAjaxPost("http://13.89.229.166/plants", payload, successfulCreate, failureCreate, newtoken)
}

function createButtonClicked(event){
    document.getElementById("plants-table").hidden = false;
    const tableBody = document.getElementById("plants-table-body");
    let newRow = document.createElement('tr');
        newRow.setAttribute('id','new-row')
    newRow.innerHTML = `<td id="new-plant-name"></td><td id="new-soil-used"></td>
                            <td id="new-fertilizer-name"></td><td id="new-days-watering"></td>
                            <td id="new-weeks-feeding"></td><td id="new-user-caption"></td>
                            <td id="new-leafs-vote" contenteditable="false"></td><td id="new-plant-id" contenteditable="false"></td>
                            <td><button contenteditable="false" style="margin:0;padding-top:0;" class="btn btn-sm btn-primary btn-block" id="create-button"  onclick="attemptCreate(this.id)">Create</button></td>
                            <td><button contenteditable="false" style="margin:0;padding-top:0;" class="btn btn-sm btn-primary btn-block" id="cancel-button"  onclick="reloadPage()">Cancel</button></td>`;
    tableBody.appendChild(newRow);
}



function successfulDelete(xhr){
    console.log(xhr)
    reloadPage();
}

function failureDelete(xhr){
    console.log(xhr)
}
function successfulUpdate(xhr){
    console.log(xhr)
    reloadPage();
}

function failureUpdate(xhr){
    console.log(xhr)
}
function successfulCreate(xhr){
    console.log("successful create")
    reloadPage();
}

function failureCreate(xhr){
    console.log("failed create")
}

function reloadPage(){
    location.reload();
}
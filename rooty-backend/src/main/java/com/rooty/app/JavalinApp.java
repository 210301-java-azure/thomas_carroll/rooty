package com.rooty.app;

import com.rooty.controllers.AuthController;
import com.rooty.controllers.PlantController;
import com.rooty.controllers.UserController;
import com.rooty.util.SecurityUtil;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;


public class JavalinApp {
    AuthController authController = new AuthController();
    UserController userController = new UserController();
    PlantController plantController = new PlantController();
    SecurityUtil securityUtil = new SecurityUtil();

    Javalin app = Javalin.create(config -> {
//        config.addSinglePageRoot("/","html/index.html");
        config.enableCorsForAllOrigins();
    }).routes(() -> {
        path("test",()->{
           get(authController::connectionTest);
        });
        path("login",()->{
            post(authController::userLogin);
            after("/", securityUtil::attachResponseHeaders);
        });
        path("register",()->{
            post(userController::create);
            after("/", securityUtil::attachResponseHeaders);
        });
        path("users",()->{
            before("/",authController::authorizeToken);
            before("/*",authController::authorizeToken);
            get(userController::getAll);
            path(":user_username", ()->{
                get(userController::getOne);
                delete(userController::delete);
                put(userController::update);
            });
        });
        path("plants",()->{
            before("/",authController::authorizeToken);
            before("/*",authController::authorizeToken);
            get(plantController::getAll);
            post(plantController::create);
            path(":plantId",()->{
                get(plantController::getOne);
                put(plantController::update);
                delete(plantController::delete);
            });
        });
        path("profile",()->{
            before("/",authController::authorizeToken);
            before("/*",authController::authorizeToken);
            path(":plant_owner", ()->{
                get(plantController::getAllPlantsByUsername);
            });
        });
    });

    public void start(int port){
        this.app.start(port);
    }


    public void stop() {this.app.stop();}

}
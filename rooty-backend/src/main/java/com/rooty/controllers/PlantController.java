package com.rooty.controllers;

import com.rooty.models.PlantItem;
import com.rooty.models.UserItem;
import com.rooty.service.PlantService;
import com.rooty.service.PlantServiceImpl;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.jetbrains.annotations.NotNull;


public class PlantController{
    PlantService plantService = new PlantServiceImpl();
    public void create(@NotNull Context context) {
        PlantItem plantItem = context.bodyAsClass(PlantItem.class);
        plantService.createPlant(plantItem);
        context.status(200);
    }

    public void delete(@NotNull Context context) {
        String idString = context.pathParam("plantId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            plantService.deletePlant(idInput);
        } else {
            context.status(400);
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

    public void getAll(@NotNull Context context) {
        context.json(plantService.getAllPlants());
        context.status(200);
    }

    public void getOne(@NotNull Context context) {
        String idString = context.pathParam("plantId");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            PlantItem item = plantService.getOnePlant(idInput);
            if (item == null) {
                throw new NotFoundResponse("No item found with provided ID: " + idInput);
            } else {
                context.json(item);
            }
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }
    }

    public void getAllPlantsByName(@NotNull Context context){
        context.json(plantService.getAllPlantsName(context.pathParam("plant_name")));
        context.status(200);
    }



    public void update(@NotNull Context context) {
        plantService.updatePlant(context.pathParam("plantId"), context.bodyAsClass(PlantItem.class));
        context.status(200);
    }

    public void getAllPlantsByUsername(Context context) {
        context.json(plantService.getAllPlantsByUsername(context.pathParam("plant_owner")));
    }
}

package com.rooty.controllers;

import com.rooty.managers.TokenManager;
import com.rooty.managers.TokenManagerImpl;
import com.rooty.models.UserItem;
import com.rooty.service.TokenService;
import com.rooty.service.TokenServiceImpl;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;

public class AuthController {
    TokenManager tokenManager = new TokenManagerImpl();

    public AuthController() {
        super();
    }

    public void authorizeToken(Context context) {
        if(context.method().equals("OPTIONS"))
            return;
        String authHeader = context.header("JWT");
        if (authHeader != null && tokenManager.authorizeToken(authHeader)) {
            context.status(200);
        } else {
            context.status(401);
            throw new UnauthorizedResponse();
        }
    }
    public void userLogin (Context context){
        TokenService tokenService = new TokenServiceImpl();
        UserItem userItem = context.bodyAsClass(UserItem.class);
        if(tokenService.userLogin(userItem)) {
            String newToken = tokenManager.issueToken(userItem.getUsername(), userItem.getPassword());
            context.header("JWT", newToken);
            context.header("USERNAME", userItem.getUsername());
            context.status(200);
            context.json(newToken);
        } else {
            context.status(401);
            throw new UnauthorizedResponse("Incorrect username or password");
        }
    }

    public void connectionTest(Context context) {
        context.result("Server was reached!");
    }
}

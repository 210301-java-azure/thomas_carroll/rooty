package com.rooty.controllers;

import com.rooty.managers.TokenManager;
import com.rooty.managers.TokenManagerImpl;
import com.rooty.models.UserItem;
import com.rooty.service.UserService;
import com.rooty.service.UserServiceImpl;
import com.microsoft.sqlserver.jdbc.StringUtils;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.jetbrains.annotations.NotNull;


public class UserController{
    UserService userService = new UserServiceImpl();
    TokenManager tokenManager = new TokenManagerImpl();

    public void create(@NotNull Context context) {
        UserItem userItem = context.bodyAsClass(UserItem.class);
        if(StringUtils.isEmpty((CharSequence) userService.getOneUser(userItem.getUsername()))){
            userService.createUser(userItem);
            String token = tokenManager.issueToken(userItem.getUsername(), userItem.getPassword());
            context.header("JWT", token);
            context.header("USERNAME", userItem.getUsername());
            context.json(token);
            context.status(200);
        } else {
            context.status(409);
            throw new UnauthorizedResponse("Username \"" + userItem.getUsername() + "\" is already taken");
        }
    }

    public void delete(@NotNull Context context) {
        userService.deleteUser(context.pathParam("user_username"));
        context.status(200);
    }

    public void getAll(@NotNull Context context) {
        context.json(userService.getAllUsers());
        context.status(200);
    }

    public void getOne(@NotNull Context context) {
        context.json(userService.getOneUser(context.pathParam("user_username")));
        context.status(200);
    }

    public void update(@NotNull Context context) {
        userService.updateUser(context.pathParam("user_username"), context.bodyAsClass(UserItem.class));
        context.status(200);
    }

}

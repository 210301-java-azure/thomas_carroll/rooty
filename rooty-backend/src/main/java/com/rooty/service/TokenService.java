package com.rooty.service;

import com.rooty.models.UserItem;

public interface TokenService {
    public Boolean userLogin(UserItem userItem);
    public Boolean authorizeToken(String password, String username);
}

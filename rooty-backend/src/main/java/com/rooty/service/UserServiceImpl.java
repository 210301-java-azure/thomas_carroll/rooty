package com.rooty.service;


import com.rooty.daos.UsersDAO;
import com.rooty.daos.UsersDAOimpl;
import com.rooty.models.UserItem;

import java.util.List;

public class UserServiceImpl implements UserService {
    private final UsersDAO usersDAO = new UsersDAOimpl();

    @Override
    public void createUser(UserItem userItem) {
        usersDAO.createUser(userItem);
    }

    @Override
    public List<UserItem> getAllUsers() {
        return usersDAO.getAllUsers();
    }

    @Override
    public UserItem getOneUser(String id) {
        return usersDAO.getOneUser(id);
    }

    @Override
    public void updateUser(String id, UserItem userItem) {
        usersDAO.updateUser(id, userItem);
    }

    @Override
    public void deleteUser(String id) {
        usersDAO.deleteUser(id);
    }
}

package com.rooty.service;

import com.rooty.models.PlantItem;

import java.util.List;
public interface PlantService {
    public void createPlant(PlantItem plantItem);
    public List<PlantItem> getAllPlants();
    public PlantItem getOnePlant(int s);
    public void updatePlant(String s, PlantItem plantItem);
    public void deletePlant(int s);

    public List<PlantItem> getAllPlantsName(String s);
    public List<PlantItem> getAllPlantsByUsername(String owner);
}

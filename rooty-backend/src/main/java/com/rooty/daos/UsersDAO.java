package com.rooty.daos;

import com.rooty.models.UserItem;
import java.util.List;

public interface UsersDAO {

    public void createUser(UserItem userItem);

    public List<UserItem> getAllUsers();

    public UserItem getOneUser(String id);

    public void updateUser(String id, UserItem userItem);

    public void deleteUser(String id);

    String getPasswordQuery(String username);
}

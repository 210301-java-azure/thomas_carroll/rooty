package com.rooty.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name="user_table")
@JsonIgnoreProperties("hibernateLazyInitializer")
public class UserItem implements Serializable {
    @Id
    @Column(name = "user_username")
    private String username;
    @Column(name = "user_password")
    private String password;
//    @Column(name = "date_joined")
//    private LocalDateTime dateJoined;
    @Column(name = "user_state")
    private String state;

    public UserItem() {
    }
    public UserItem(String id){super();}

    public UserItem(String username, String password, String state) {
        this.username = username;
        this.password = password;
        this.state = state;
    }
    public UserItem(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername() {return username;}
    public void setUsername(String username) {this.username = username;}
    public String getPassword() {return password;}
    public void setPassword(String password) {this.password = password;}
//    public LocalDateTime getDateJoined() {return dateJoined;}
//    public void setDateJoined(LocalDateTime dateJoined) {this.dateJoined = dateJoined;}
    public String getState() {return state;}
    public void setState(String state) {this.state = state;}

}
